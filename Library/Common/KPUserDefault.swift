//
//  KPUserDefault.swift
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/10/16.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

import Foundation

class KPUserDefault: NSObject {

    private var userDefault = UserDefaults(suiteName: KPApplicationSuiteName)!

    @objc class var standardUserDefaults: UserDefaults {
        return KPUserDefault().userDefault
    }
}
