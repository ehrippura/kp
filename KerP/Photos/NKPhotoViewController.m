//
//  NKPhotoViewController.m
//  NaviKing2
//
//  Created by Tzu-Yi Lin on 2014/2/13.
//  Copyright (c) 2014年 KingwayTek. All rights reserved.
//

#import "NKPhotoViewController.h"

#import "AFNetworking.h"

#pragma mark - Inner View

static NSCache *imageCache;
static NSMutableSet *loadingImage;

@interface UIScrollView (center)
- (void)centerContent;
@end

@implementation UIScrollView (center)

- (void)centerContent
{
    CGFloat top = 0, left = 0;
    if (self.contentSize.width < self.bounds.size.width) {
        left = (self.bounds.size.width - self.contentSize.width) * 0.5f;
    }
    if (self.contentSize.height < self.bounds.size.height) {
        top = (self.bounds.size.height - self.contentSize.height) * 0.5f;
    }
    self.contentInset = UIEdgeInsetsMake(top, left, top, left);
}

@end

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

@interface NKPhotoViewControllerContainView : UICollectionViewCell <UIScrollViewDelegate> {
    UIImage *_image;

    UIImageView *_photoView;
    UIScrollView *_scrollView;
    UIActivityIndicatorView *_networkIndicator;

    BOOL _tapGestureFire;
}

@property (nonatomic, strong, readonly) UILabel *titleLabel;
@property (nonatomic, strong, readonly) UILabel *detailLabel;
@property (nonatomic, strong, readonly) UIView *labelBackground;
@property (nonatomic, strong, readonly) UIActivityIndicatorView *networkIndicator;
@property (nonatomic, assign) BOOL detailVisible;

- (void)setImage:(UIImage *)image;
- (void)setDetailVisible:(BOOL)visible;

@end

@implementation NKPhotoViewControllerContainView

@synthesize titleLabel = _titleLabel;
@synthesize detailLabel = _detailLabel;
@synthesize labelBackground = _labelBackground;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // init required subviews
        _photoView = [[UIImageView alloc] initWithFrame:frame];

        _networkIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _networkIndicator.hidesWhenStopped = YES;
        _networkIndicator.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
        _networkIndicator.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);

        _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        _scrollView.delegate = self;
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _scrollView.maximumZoomScale = 3.0;
        _scrollView.minimumZoomScale = 1.0;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        [_scrollView addSubview:_photoView];

        [self addSubview:_scrollView];
        [self addSubview:_networkIndicator];

        // tap gesture
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
        [self addGestureRecognizer:tapGesture];

        [self prepareForReuse];
    }
    return self;
}

- (void)tapGesture:(UITapGestureRecognizer *)gesture
{
    if (!_titleLabel && !_detailLabel)
        return;

    // do not check text first in this method
    // prevent label creation
    if (!self.titleLabel.text && !self.detailLabel.text)
        return;

    _tapGestureFire = YES;
    self.detailVisible = !self.detailVisible;
    if (self.detailVisible) [self adjustDetailView];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self.networkIndicator stopAnimating];
    _photoView.image = nil;
    self.labelBackground.alpha = 0;
    self.detailVisible = YES;
    _tapGestureFire = NO;
}

- (UIView *)labelBackground
{
    if (!_labelBackground) {
        // create label background
        _labelBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
        _labelBackground.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
        _labelBackground.userInteractionEnabled = NO;
        _labelBackground.alpha = 0;

        // create title label
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLabel.textColor = [UIColor whiteColor];
        [self.labelBackground addSubview:_titleLabel];

        // create detail label
        _detailLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _detailLabel.numberOfLines = 0;
        _detailLabel.textColor = [UIColor whiteColor];
        [self.labelBackground addSubview:_detailLabel];

        [self addSubview:_labelBackground];
    }

    return _labelBackground;
}

- (UILabel *)titleLabel
{
    [self labelBackground];
    return _titleLabel;
}

- (UILabel *)detailLabel
{
    [self labelBackground];
    return _detailLabel;
}

- (void)setDetailVisible:(BOOL)visible
{
    _detailVisible = visible;

    [UIView animateWithDuration:0.3 animations:^{
        self.labelBackground.alpha = (visible) ? 1 : 0;
    }];
}

#define MERGE_SIZE  10
- (void)adjustDetailView
{
    CGFloat width = self.bounds.size.width;
    CGFloat labelWidth = width - MERGE_SIZE * 2;

    CGSize titleSize = [_titleLabel.text boundingRectWithSize:CGSizeMake(labelWidth, MAXFLOAT)
                                                      options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                   attributes:@{NSFontAttributeName: _titleLabel.font} context:nil].size;

    CGSize detailSize = [_detailLabel.text boundingRectWithSize:CGSizeMake(labelWidth, MAXFLOAT)
                                                        options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                     attributes:@{NSFontAttributeName: _detailLabel.font} context:nil].size;

    CGFloat height = ceilf(titleSize.height + detailSize.height + MERGE_SIZE * 2);

    _detailLabel.frame = CGRectMake(MERGE_SIZE, MERGE_SIZE, labelWidth, ceilf(detailSize.height));
    _titleLabel.frame = CGRectMake(MERGE_SIZE, CGRectGetMaxY(_detailLabel.frame) + ((detailSize.height != 0) ? MERGE_SIZE : 0), labelWidth, ceilf(titleSize.height));

    self.labelBackground.frame = CGRectMake(0, self.bounds.size.height - height, width, height);
}

- (void)setImage:(UIImage *)image
{
    _photoView.image = image;
    [self setNeedsLayout];
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    // prevent resize after user tap the image
    if (_tapGestureFire) {
        _tapGestureFire = NO;
        return;
    }

    UIImage *image = _photoView.image;

    if (image) {
        CGSize scrollSize = _scrollView.bounds.size;

        if (image.size.width <= scrollSize.width && image.size.height <= scrollSize.height) {
            _scrollView.zoomScale = 1;
            _photoView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
        } else {
            CGFloat widthRadio = scrollSize.width / image.size.width;
            CGFloat heightRadio = scrollSize.height / image.size.height;
            CGFloat initRadio = (widthRadio < heightRadio) ? widthRadio : heightRadio;
            CGSize imageSize = image.size;
            _scrollView.zoomScale = initRadio;

            _photoView.frame = CGRectMake(0, 0, imageSize.width * initRadio, imageSize.height * initRadio);
        }

        _scrollView.contentSize = _photoView.frame.size;
        [_scrollView centerContent];
    }

    if (self.detailVisible) [self adjustDetailView];
}

// scroll delegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _photoView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    scrollView.contentSize = _photoView.frame.size;
    [scrollView centerContent];
}


@end

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#pragma mark - NKPhotoViewController Implementation

@interface NKPhotoViewController () <UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {

    UIActivityIndicatorView *_networkIndicator;
    NSArray *_contentViews; // three contant view

    NSUInteger _numberOfImages;
    NSUInteger _currentIndex;
    NSInteger _preparePresentIndex;

    UICollectionViewFlowLayout *_layout;
    UILabel *_countLabel;
}

@end

@implementation NKPhotoViewController

+ (void)initialize
{
    if (!imageCache) {
        imageCache = [[NSCache alloc] init];
        [imageCache setCountLimit:20];
    }

    if (!loadingImage) {
        loadingImage = [[NSMutableSet alloc] init];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // create paging scroll view
        _layout = [[UICollectionViewFlowLayout alloc] init];
        _layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;

        self.photoMergin = 10;
        _preparePresentIndex = -1;
        _currentIndex = 0;
    }
    return self;
}

- (void)loadView
{
    [super loadView];

    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:_layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.pagingEnabled = YES;
    self.collectionView.backgroundColor = [UIColor blackColor];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;

    [self.view addSubview:self.collectionView];

    // add constraints
    NSMutableArray *constraints = [NSMutableArray array];
    [constraints addObjectsFromArray:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(gap)-[_collectionView]-(gap)-|" options:0 metrics:@{@"gap": @(-self.photoMergin)} views:NSDictionaryOfVariableBindings(_collectionView)]];

    [constraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[_collectionView]-(0)-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_collectionView)]];

    [self.view addConstraints:constraints];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self reloadData];
    // register collection cell
    [self.collectionView registerClass:[NKPhotoViewControllerContainView class] forCellWithReuseIdentifier:@"Cell"];
    self.view.backgroundColor = [UIColor blackColor];

    self.title = @"照片";

    _countLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 21)];
    _countLabel.textColor = [UIColor whiteColor];
    _countLabel.textAlignment = NSTextAlignmentRight;

    UIBarButtonItem *labelButton = [[UIBarButtonItem alloc] initWithCustomView:_countLabel];
    self.navigationItem.rightBarButtonItem = labelButton;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    _layout.minimumLineSpacing = self.photoMergin * 2;
    _layout.sectionInset = UIEdgeInsetsMake(0, self.photoMergin, 0, self.photoMergin);
    [_layout invalidateLayout];

    self.collectionView.alpha = 0;

    _countLabel.text = @"";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self reloadData];
    if (_preparePresentIndex != -1) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_preparePresentIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }
    self.collectionView.alpha = 1;
    _countLabel.text = (_numberOfImages != 1) ? [NSString stringWithFormat:@"%ld/%ld", (long)(_currentIndex + 1), (long)(_numberOfImages)] : @"";
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscape;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [_layout invalidateLayout];
    self.collectionView.alpha = 0;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self reloadData];
    self.collectionView.alpha = 1;
}

- (void)reloadData
{
    _numberOfImages = [self.dataSource numberOfPhotosForController:self];

    if (_numberOfImages <= _currentIndex) {
        _currentIndex = _numberOfImages - 1;
    }

    [self.collectionView reloadData];
    [self presentImageAtIndex:_currentIndex animated:NO];
}

- (NSUInteger)centerIndex
{
    return _currentIndex;
}

- (void)presentImageAtIndex:(NSUInteger)index animated:(BOOL)animated
{
    if ([self isViewLoaded] &&  index < (NSUInteger)[self.collectionView numberOfItemsInSection:0]) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:animated];
    } else {
        _preparePresentIndex = index;
        _currentIndex = _preparePresentIndex;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.dataSource numberOfPhotosForController:self];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NKPhotoViewControllerContainView *contentView = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];

    NSInteger index = indexPath.item;
    id image = [self.dataSource imageForIndex:index];

    if (image) {
        if ([image isKindOfClass:[UIImage class]]) {
            [contentView setImage:image];
        } else {
            [contentView.networkIndicator stopAnimating];
            UIImage *cacheImage = [imageCache objectForKey:image];
            if (cacheImage)
                [contentView setImage:cacheImage];
            else {
                if (![loadingImage containsObject:image]) {
                    [loadingImage addObject:image];
                    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                    manager.responseSerializer = [AFImageResponseSerializer serializer];
                    [manager GET:image parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        NSAssert([responseObject isKindOfClass:[UIImage class]], @"response class error");

                        if (self.dataSource) {
                            [imageCache setObject:responseObject forKey:image];
                            [collectionView reloadItemsAtIndexPaths:@[indexPath]];
                            [loadingImage removeObject:image];
                        }

                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        NSLog(@"%@", error);
                        [loadingImage removeObject:image];
                    }];
                }
                [contentView.networkIndicator startAnimating];
            }
        }
    }

    if ([self.dataSource respondsToSelector:@selector(titleForPhotoAtIndex:)]) {
        NSString *title = [self.dataSource titleForPhotoAtIndex:index];
        if (title) contentView.titleLabel.text = title;
    }

    if ([self.dataSource respondsToSelector:@selector(detailForPhotoAtIndex:)]) {
        NSString *detail = [self.dataSource detailForPhotoAtIndex:index];
        if (detail) contentView.detailLabel.text = detail;
    }

    return contentView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger index = scrollView.contentOffset.x / scrollView.bounds.size.width;
    _countLabel.text = (_numberOfImages != 1) ? [NSString stringWithFormat:@"%ld/%ld", (long)(index + 1), (long)_numberOfImages] : @"";

    _currentIndex = index;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    CGSize size = collectionView.frame.size;
    size.width -= self.photoMergin * 2;
    size.height -= collectionView.contentInset.top + collectionView.contentInset.bottom;

    return size;
}

@end
