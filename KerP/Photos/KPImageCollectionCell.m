//
//  KPImageCollectionCell.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/25.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import "KPImageCollectionCell.h"
#import "KPNetworkIndicator.h"

#import "UIImageView+AFNetworking.h"

@implementation KPImageCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

- (void)setImageURL:(NSURL *)url
{
    [_networkIndicator startAnimation];
    if (url) {

        __weak KPNetworkIndicator *indicator = _networkIndicator;
        __weak UIImageView *imageView = _imageView;

        [_imageView setImageWithURLRequest:[NSURLRequest requestWithURL:url] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            indicator.hidden = YES;
            [indicator stopAnimation];
            imageView.image = image;
            imageView.alpha = 0;

            [UIView animateWithDuration:0.2 animations:^{
                imageView.alpha = 1;
            }];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            [indicator stopAnimation];
        }];
    }
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [_networkIndicator stopAnimation];
    _networkIndicator.hidden = NO;
    [_imageView setImage:nil];
}

@end
