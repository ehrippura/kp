//
//  KPPhotoCollectionViewController.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/25.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import "KPPhotoCollectionViewController.h"
#import "KPImageCollectionCell.h"

#import "KPAlbumItem.h"
#import "KPNetworkManager.h"
#import "KPPhotoItem.h"

#import "NKPhotoListControllerImageSource.h"
#import "NKPhotoViewController.h"

@interface KPPhotoCollectionViewController() <NKPhotoViewControllerDataSource> {
    NSMutableArray *photoDatas;
    NSInteger page;

    BOOL hasData;
}

@end

@implementation KPPhotoCollectionViewController

@synthesize albumItem = _albumItem;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];

    if (self) {
        self.photoNumberPredication = 0;
        page = 0;
        hasData = NO;
    }

    return self;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)setAlbumItem:(KPAlbumItem *)albumItem
{
    if (albumItem != _albumItem) {
        [self willChangeValueForKey:@"albumItem"];

        _albumItem = albumItem;
        self.photoNumberPredication = albumItem.numberOfPhotos;
        self.title = albumItem.title;
        [self performNetworkRequest];

        photoDatas = [[NSMutableArray alloc] initWithCapacity:self.photoNumberPredication];

        [self didChangeValueForKey:@"albumItem"];
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    CGFloat width = floorf((self.view.bounds.size.width - 20) / 3);
    [(UICollectionViewFlowLayout *)self.collectionViewLayout setItemSize:CGSizeMake(width, width)];
}

- (void)performNetworkRequest
{
    [KPNetworkManager requirePhotoListForAlbumID:_albumItem.albumID
                                      pageNumber:page++
                                 completehandler:^(NSArray *objectList, NSError *error, BOOL hasMore) {
        if (!error) {
            hasData = YES;
            NSInteger photoCount = [photoDatas count];
            [photoDatas addObjectsFromArray:objectList];
            [self adjustPredicationToPhotoNumber:[photoDatas count] reduceSize:NO];

            NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:[objectList count]];

            for (NSInteger i = photoCount; i < photoDatas.count; i++) {
                [indexPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
            }

            [self.collectionView reloadItemsAtIndexPaths:indexPaths];

            if (hasMore) {
                [self performNetworkRequest];
            }
        };
    }];
}

- (void)adjustPredicationToPhotoNumber:(NSInteger)count reduceSize:(BOOL)reduce
{
    if ([photoDatas count] > self.photoNumberPredication) {
        NSMutableArray *newItemIndexPath = [[NSMutableArray alloc] init];

        for (NSInteger i = self.photoNumberPredication; i < photoDatas.count; i++) {
            [newItemIndexPath addObject:[NSIndexPath indexPathForItem:i inSection:0]];
        }

        self.photoNumberPredication = [photoDatas count];
        [self.collectionView insertItemsAtIndexPaths:newItemIndexPath];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.photoNumberPredication != 0)
        return self.photoNumberPredication;

    return [photoDatas count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    KPImageCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];

    @try {
        KPPhotoItem *photoItem = [photoDatas objectAtIndex:indexPath.row];
        NSURL *url = [NSURL URLWithString:photoItem.thumbnailURL];
        [cell setImageURL:url];
    }
    @catch (NSException *exception) {
        [cell setImageURL:nil];
    }

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!hasData) return;
    
    NKPhotoViewController *viewController = [[NKPhotoViewController alloc] initWithNibName:nil bundle:nil];

    [viewController presentImageAtIndex:indexPath.row animated:NO];
    viewController.dataSource = self;

    [self.navigationController pushViewController:viewController animated:YES];
}

// data source
- (NSUInteger)numberOfPhotosForController:(NKPhotoViewController *)photoController
{
    return self.photoNumberPredication;
}

- (id)imageForIndex:(NSUInteger)index
{
    @try {
        KPPhotoItem *photoItem = [photoDatas objectAtIndex:index];
        return photoItem.originalImageURL;
    }
    @catch (NSException *exception) {
        return nil;
    }
}

@end
