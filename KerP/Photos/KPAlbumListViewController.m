//
//  KPViewController.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/21.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import "KPAlbumListViewController.h"

#import "UIImageView+AFNetworking.h"
#import "KPNetworkManager.h"
#import "KPAlbumItem.h"

#import "KPAlbumListCell.h"
#import "KPPhotoCollectionViewController.h"

#import "NSIndexPath+KPCategory.h"

@interface KPAlbumListViewController () {
    BOOL loading;
    BOOL hasMoreAlbum;

    NSMutableArray *albumList;
    NSInteger loadTargetPage;
}

@end

@implementation KPAlbumListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 75, 0, 0);
    self.tableView.separatorColor = [UIColor colorWithRed:0.519f green:0.239f blue:0.076f alpha:1.000f];

    // add refresh handler
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];

    loading = NO;
    hasMoreAlbum = YES;
    loadTargetPage = 0;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if ([albumList count] == 0) {
        [self loadDataFromNetwork];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadDataFromNetwork
{
    if (loading) return;

    loading = YES;
    [KPNetworkManager requireAlbumListForPage:loadTargetPage++ completeHandler:^(NSArray *objectList, NSError *error, BOOL hasMore) {
        if (!error) {
            if ([albumList count] == 0) {
                albumList = [objectList mutableCopy];
                [self.tableView reloadData];
            } else {
                NSInteger numberOfItemAdded = 0;
                NSInteger originalAlbumCount = [albumList count];

                for (id albumItem in objectList) {
                    if (![albumList containsObject:albumItem]) {
                        [albumList addObject:albumItem];
                        numberOfItemAdded++;
                    }
                }

                NSArray *indexPaths = [NSIndexPath indexPathsForSection:0 fromRow:originalAlbumCount length:numberOfItemAdded];
                [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            }

            hasMoreAlbum = hasMore;
            loading = NO;
        }
    }];
}

- (void)refresh:(id)sender
{
    [sender endRefreshing];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [albumList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KPAlbumListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableCell" forIndexPath:indexPath];

    KPAlbumItem *item = albumList[indexPath.row];
    [cell.titleImage setImageWithURL:[NSURL URLWithString:item.thumbnailImageLocation]];
    [cell.titleLabel setText:item.title];

    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // cell animation
    KPAlbumListCell *albumCell = (KPAlbumListCell *)cell;

    albumCell.titleLabel.transform = CGAffineTransformMakeTranslation(40, 0);
    albumCell.titleLabel.alpha = 0;

    albumCell.titleImage.transform = CGAffineTransformMakeTranslation(-70, 0);
    albumCell.titleImage.alpha = 0;

    [UIView animateWithDuration:0.5 animations:^{
        albumCell.titleLabel.transform = CGAffineTransformIdentity;
        albumCell.titleLabel.alpha = 1;
        albumCell.titleImage.transform = CGAffineTransformIdentity;
        albumCell.titleImage.alpha = 1;
    }];

    // load data
    if (indexPath.row >= albumList.count - 4 && hasMoreAlbum) {
        [self loadDataFromNetwork];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

// selection

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *identifier = segue.identifier;

    KPAlbumItem *albumItem = albumList[[self.tableView indexPathForSelectedRow].row];

    if ([identifier isEqualToString:@"albumDetailPage"]) {
        KPPhotoCollectionViewController *controller = segue.destinationViewController;
        controller.albumItem = albumItem;
    }
}

@end
