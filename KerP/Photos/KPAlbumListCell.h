//
//  KPAlbumListCell.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/25.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPAlbumListCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *errorImage;
@property (nonatomic, weak) IBOutlet UIImageView *titleImage;

@end
