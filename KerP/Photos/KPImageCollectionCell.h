//
//  KPImageCollectionCell.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/25.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KPNetworkIndicator;

@interface KPImageCollectionCell : UICollectionViewCell {
    IBOutlet KPNetworkIndicator *_networkIndicator;
    IBOutlet UIImageView *_imageView;
}

- (void)setImageURL:(NSURL *)url;

@end
