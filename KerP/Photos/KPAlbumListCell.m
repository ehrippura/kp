//
//  KPAlbumListCell.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/25.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import "KPAlbumListCell.h"

@implementation KPAlbumListCell

- (void)awakeFromNib
{
    self.titleImage.layer.cornerRadius = self.titleImage.bounds.size.width / 2;
    self.titleImage.layer.masksToBounds = YES;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.titleImage.image = nil;
}

@end
