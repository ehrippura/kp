//
//  NKPhotoListControllerImageSource.m
//  NaviKing2
//
//  Created by Tzu-Yi Lin on 2014/2/17.
//  Copyright (c) 2014年 KingwayTek. All rights reserved.
//

#import "NKPhotoListControllerImageSource.h"

@implementation NKPhotoListControllerImageSource

@synthesize image = _image;

- (id)initWithImage:(UIImage *)image
{
    self = [super init];
    if (self) {
        self.image = image;
    }
    return self;
}

- (id)initWithImageURL:(NSURL *)imageURL
{
    if (self = [super init]) {
        self.imageURL = imageURL;
    }
    return self;
}

@end
