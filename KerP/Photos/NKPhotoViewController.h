//
//  NKPhotoViewController.h
//  NaviKing2
//
//  Created by Tzu-Yi Lin on 2014/2/13.
//  Copyright (c) 2014年 KingwayTek. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NKPhotoListControllerImageSource.h"

@protocol NKPhotoViewControllerDataSource;

@interface NKPhotoViewController : UIViewController

@property (nonatomic, weak) id <NKPhotoViewControllerDataSource> dataSource;

@property (nonatomic, assign) CGFloat photoMergin;
@property (nonatomic, strong) UICollectionView *collectionView;

- (void)reloadData;
- (void)presentImageAtIndex:(NSUInteger)index animated:(BOOL)animated;

@end


@protocol NKPhotoViewControllerDataSource <NSObject>

@required
- (NSUInteger)numberOfPhotosForController:(NKPhotoViewController *)photoController;
// return UIImage or NSString (url)
- (id)imageForIndex:(NSUInteger)index;

@optional
- (NSString *)titleForPhotoAtIndex:(NSUInteger)index;
- (NSString *)detailForPhotoAtIndex:(NSUInteger)index;


@end
