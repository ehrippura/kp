//
//  KPPhotoCollectionViewController.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/25.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KPAlbumItem;

@interface KPPhotoCollectionViewController : UICollectionViewController

@property (nonatomic, assign) NSInteger photoNumberPredication;
@property (nonatomic, weak) KPAlbumItem *albumItem;

@end
