//
//  NKPhotoListControllerImageSource.h
//  NaviKing2
//
//  Created by Tzu-Yi Lin on 2014/2/14.
//  Copyright (c) 2014年 KingwayTek. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NKPhotoListControllerImageSource <NSObject>
@required
@property (nonatomic, strong) UIImage *image;
@end


// image source object
@interface NKPhotoListControllerImageSource : NSObject <NKPhotoListControllerImageSource>

@property (nonatomic, copy) NSURL *imageURL;

- (id)initWithImage:(UIImage *)image;
- (id)initWithImageURL:(NSURL *)imageURL;

@end

