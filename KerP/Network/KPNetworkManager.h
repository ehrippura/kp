//
//  KPNetworkManager.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/21.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSErrorDomain KPNetworkErrorDomain;

@class AFHTTPRequestOperationManager;

typedef void (^KPNetworkRequireListCompleteHandler)(NSArray *objectList, NSError *error, BOOL hasMore);

@interface KPNetworkManager : NSObject

@property (nonatomic, strong, readonly) AFHTTPRequestOperationManager *manager;


+ (instancetype)sharedManager;

// album
+ (void)requireAlbumListForPage:(NSInteger)page completeHandler:(KPNetworkRequireListCompleteHandler)handler;
+ (void)requirePhotoListForAlbumID:(NSString *)albumID
                        pageNumber:(NSInteger)page
                   completehandler:(KPNetworkRequireListCompleteHandler)handler;

// category
+ (void)requireCategoryWithCompleteHandler:(KPNetworkRequireListCompleteHandler)handler;
+ (void)requireItemsForCategoryID:(NSNumber *)albumID
                       pageNumber:(NSInteger)page
                  completehandler:(KPNetworkRequireListCompleteHandler)handler;

+ (NSURL *)categoryDataURL;

@end
