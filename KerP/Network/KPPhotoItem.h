//
//  KPPhotoItem.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/26.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KPPhotoItem : NSObject

@property (nonatomic, copy) NSString *thumbnailURL;
@property (nonatomic, copy) NSString *originalImageURL;

@end
