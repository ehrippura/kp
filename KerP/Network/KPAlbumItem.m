//
//  KPAlbumItem.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/25.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import "KPAlbumItem.h"

@implementation KPAlbumItem

- (BOOL)isEqual:(id)object
{
    return [self.albumID isEqualToString:[object albumID]];
}

@end
