//
//  KPCategoryItem.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/1.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KPCategory : NSObject <NSCopying, NSCoding>

@property (nonatomic, copy) NSNumber *itemID;
@property (nonatomic, copy) NSString *name;

@end


@interface KPCategoryItem : NSObject <NSCoding>

@property (nonatomic, copy) NSNumber *itemID;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *categoryName;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *htmlContent;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSDate *createDate;
@property (nonatomic, strong) NSDate *modifyDate;
@property (nonatomic, copy) NSString *thumbnail;
@property (nonatomic, copy) NSString *pageURL;

- (NSString *)trimedTitle;
- (NSString *)linksRemovedContent;
- (NSString *)videoID;

@end

