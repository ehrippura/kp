//
//  KPCategoryItem.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/1.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "KPCategoryItem.h"

@implementation KPCategory

- (id)copyWithZone:(NSZone *)zone
{
    KPCategory *item = [KPCategory allocWithZone:zone];

    item.itemID = [self.itemID copy];
    item.name = [self.name copy];

    return item;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    if (self) {
        self.itemID = [aDecoder decodeObjectForKey:@"itemid"];
        self.name = [aDecoder decodeObjectForKey:@"name"];
    }

    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.itemID forKey:@"itemid"];
    [aCoder encodeObject:self.name forKey:@"name"];
}

- (NSUInteger)hash
{
    return [self.itemID hash] + [self.name hash];
}

@end



@implementation KPCategoryItem

// for Coding
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.itemID = [aDecoder decodeObjectForKey:@"itemID"];
        self.author = [aDecoder decodeObjectForKey:@"author"];
        self.categoryName = [aDecoder decodeObjectForKey:@"categoryName"];
        self.content = [aDecoder decodeObjectForKey:@"content"];
        self.htmlContent = [aDecoder decodeObjectForKey:@"htmlContent"];
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.createDate = [aDecoder decodeObjectForKey:@"createDate"];
        self.modifyDate = [aDecoder decodeObjectForKey:@"modifyDate"];
        self.thumbnail = [aDecoder decodeObjectForKey:@"thumbnail"];
        self.pageURL = [aDecoder decodeObjectForKey:@"pageURL"];
    }

    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.itemID forKey:@"itemID"];
    [aCoder encodeObject:self.author forKey:@"author"];
    [aCoder encodeObject:self.categoryName forKey:@"categoryName"];
    [aCoder encodeObject:self.content forKey:@"content"];
    [aCoder encodeObject:self.htmlContent forKey:@"htmlContent"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.createDate forKey:@"createDate"];
    [aCoder encodeObject:self.modifyDate forKey:@"modifyDate"];
    [aCoder encodeObject:self.thumbnail forKey:@"thumbnail"];
    [aCoder encodeObject:self.pageURL forKey:@"pageURL"];
}

- (BOOL)isEqual:(id)object
{
    return [self.itemID isEqualToNumber:[object itemID]];
}

- (NSString *)trimedTitle
{
    NSRange range = [self.title rangeOfString:self.categoryName];
    NSString *targetString = self.title;
    if (range.location != NSNotFound) {
        targetString = [self.title stringByReplacingCharactersInRange:range withString:@""];
    }

    return [targetString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)linksRemovedContent
{
    NSString *targetString = self.content;
    NSRange range = [targetString rangeOfString:@"\\[http[^\\]]*]" options:NSRegularExpressionSearch];

    while (range.location != NSNotFound) {
        targetString = [targetString stringByReplacingCharactersInRange:range withString:@""];
        range = [targetString rangeOfString:@"\\[http[^\\]]*]" options:NSRegularExpressionSearch];
    }

    return [targetString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];;
}

- (NSString *)videoID
{
    NSRange range = [self.content rangeOfString:@"\\[http[^\\]]*]" options:NSRegularExpressionSearch];

    if (range.location != NSNotFound) {
        range.location += 1;
        range.length -= 2;
        NSString *url = [self.content substringWithRange:range];

        range = [url rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"/="] options:NSBackwardsSearch];

        NSString *code = [url substringFromIndex:range.location + 1];
        return code;
    }

    return nil;
}

@end


