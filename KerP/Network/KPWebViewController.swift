//
//  KPWebViewController.swift
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/12/2.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

import UIKit

class KPWebViewController: UIViewController, UIWebViewDelegate {

  @IBOutlet var webView: UIWebView!

  @objc var articalTitle: String?
  @objc var urlString: String? {
    didSet {
      if isViewLoaded {
        if let url = urlString {
          webView.loadRequest(URLRequest(url: URL(string: url)!))
        }
      }
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    if let url = urlString {
      webView.loadRequest(URLRequest(url: URL(string: url)!))
    }
  }

  var originalBarTintColor: UIColor?
  var originalTintColor: UIColor?
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    originalBarTintColor = navigationController?.navigationBar.barTintColor
    originalTintColor = navigationController?.navigationBar.tintColor
    navigationController?.navigationBar.barTintColor = UIColor(white: 0.9, alpha: 1)
    navigationController?.navigationBar.tintColor = UIColor.darkGray
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    navigationController?.navigationBar.barTintColor = originalBarTintColor
    navigationController?.navigationBar.tintColor = originalTintColor
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func shareButtonDown(sender: AnyObject) {
    var shareItems: [AnyObject] = []

    if let actTitle = articalTitle {
      shareItems.append(actTitle as AnyObject)
    }

    if let url = NSURL(string: urlString!) {
      shareItems.append(url)
    }

    let activityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
    present(activityViewController, animated: true, completion: nil)
  }
}
