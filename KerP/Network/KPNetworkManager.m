//
//  KPNetworkManager.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/21.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import "KPNetworkManager.h"
#import "AFNetworking.h"

#import "KPAlbumItem.h"
#import "KPPhotoItem.h"
#import "KPCategoryItem.h"

// url definition
#define KP_TAIPEI_BASE_URL          @"http://api.kptaipei.tw/"
#define KP_TAIPEI_API_VERSION       @"v1"

// API definition
#define API_ALBUM           @"albums/%@"
#define API_CATEGORY        @"category/%@"
#define API_KEY             @"kp53fc3252eb3316.33917438"

#define KP_ESCAPE_NSNULL(x)    [self checkEmptyValue:(x)]

NSErrorDomain KPNetworkErrorDomain = @"tw.eternalwind.networkError";

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

@interface NSDictionary(AppendAccessToken)

- (NSDictionary *)kp_appendToken;
- (BOOL)kp_responseSuccess;
- (NSInteger)kp_responseErrorCode;
- (NSString *)kp_responseErrorMessage;

@end

@implementation NSDictionary(AppendAccessToken)

- (NSDictionary *)kp_appendToken;
{
    NSMutableDictionary *mut = [self mutableCopy];
    [mut setObject:API_KEY forKey:@"accessToken"];

    return [mut copy];
}

- (BOOL)kp_responseSuccess
{
    return [[self objectForKey:@"isSuccess"] boolValue];
}

- (NSInteger)kp_responseErrorCode
{
    return [[self objectForKey:@"errorCode"] integerValue];
}

- (NSString *)kp_responseErrorMessage
{
    return [self objectForKey:@"errorMessage"];
}

@end


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define LOAD_PER_PAGE  20

@implementation KPNetworkManager

@synthesize manager = _manager;

+ (instancetype)sharedManager
{
    static id master = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        master = [[self alloc] init];
    });

    return master;
}

+ (instancetype)checkEmptyValue:(id)value
{
    if (value != [NSNull null])
        return value;

    return nil;
}

+ (void)requireAlbumListForPage:(NSInteger)page
                completeHandler:(KPNetworkRequireListCompleteHandler)handler
{
    NSDictionary *request = @{@"page": @(page), @"page_size": @(LOAD_PER_PAGE)};
    [[[self sharedManager] manager] GET:[NSString stringWithFormat:API_ALBUM, @""] parameters:[request kp_appendToken] success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {

        if ([responseObject kp_responseSuccess]) {
            NSArray *albums = responseObject[@"data"];
            NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:[albums count]];

            NSString * (^processTitle)(NSString *) = ^NSString *(NSString *title) {
                NSRange range = [title rangeOfString:@"\\d+\\.\\d+\\.\\d+\\.*" options:NSRegularExpressionSearch];
                NSString *substring = [title substringFromIndex:range.length];
                return ([substring length] > 0) ? substring : title;
            };

            for (NSDictionary *item in albums) {
                KPAlbumItem *albumItem = [KPAlbumItem new];

                albumItem.albumID = item[@"id"];
                albumItem.title = processTitle(item[@"title"]);
                albumItem.createDate = [NSDate dateWithTimeIntervalSince1970:[item[@"date_create"] doubleValue] / 1000];
                albumItem.updateDate = [NSDate dateWithTimeIntervalSince1970:[item[@"date_update"] doubleValue] / 1000];
                albumItem.numberOfPhotos = [item[@"photos"] integerValue];
                albumItem.numberOfVideos = [item[@"videos"] integerValue];
                albumItem.thumbnailImageLocation = [item valueForKeyPath:@"thumbnails.large_square"];

                [dataArray addObject:albumItem];
            }

            [dataArray sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"updateDate" ascending:NO]]];

            NSInteger totalResult = [[responseObject valueForKeyPath:@"pageInfo.totalResults"] integerValue];
            handler([dataArray copy], nil, ((page + 1) * LOAD_PER_PAGE) < totalResult);

        } else {
            handler(nil, [NSError errorWithDomain:KPNetworkErrorDomain code:[responseObject kp_responseErrorCode] userInfo:nil], NO);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(nil, error, NO);
    }];
}

+ (void)requirePhotoListForAlbumID:(NSString *)albumID
                        pageNumber:(NSInteger)page
                   completehandler:(KPNetworkRequireListCompleteHandler)handler
{
    NSDictionary *request = @{@"page": @(page), @"page_size":@(LOAD_PER_PAGE)};
    NSString *apiTarget = [NSString stringWithFormat:API_ALBUM, albumID];

    [[[self sharedManager] manager] GET:apiTarget parameters:[request kp_appendToken] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject kp_responseSuccess]) {
            NSArray *photos = [responseObject valueForKeyPath:@"data.photos"];
            NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:[photos count]];

            for (NSDictionary *photoDictionary in photos) {
                KPPhotoItem *item = [KPPhotoItem new];

                item.thumbnailURL = [photoDictionary valueForKeyPath:@"images.large_square"];
                item.originalImageURL = [photoDictionary valueForKeyPath:@"images.large"];

                [dataArray addObject:item];
            }

            NSInteger totalResult = [[responseObject valueForKeyPath:@"pageInfo.totalResults"] integerValue];

            handler(dataArray, nil, ((page + 1) * LOAD_PER_PAGE) < totalResult);
        } else {
            handler(nil, [NSError errorWithDomain:KPNetworkErrorDomain code:[responseObject kp_responseErrorCode] userInfo:nil], NO);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(nil, error, NO);
    }];
}

+ (void)requireCategoryWithCompleteHandler:(KPNetworkRequireListCompleteHandler)handler
{
    NSString *apiTarget = [NSString stringWithFormat:API_CATEGORY, @""];
    [[[self sharedManager] manager] GET:apiTarget parameters:[@{} kp_appendToken] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject kp_responseSuccess]) {
            NSArray *categories = [responseObject objectForKey:@"data"];
            NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:[categories count]];

            for (NSDictionary *dataDictionary in categories) {
                KPCategory *item = [KPCategory new];

                item.itemID = dataDictionary[@"id"];
                item.name = dataDictionary[@"name"];

                [dataArray addObject:item];
            }
            handler(dataArray, nil, NO);
        } else {
            handler(nil, [NSError errorWithDomain:KPNetworkErrorDomain code:[responseObject kp_responseErrorCode] userInfo:nil], NO);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(nil, error, NO);
    }];
}

+ (void)requireItemsForCategoryID:(NSNumber *)categoryID
                       pageNumber:(NSInteger)page
                  completehandler:(KPNetworkRequireListCompleteHandler)handler
{
    NSDictionary *request = @{@"page": @(page), @"page_size":@(LOAD_PER_PAGE)};
    NSString *apiTarget = [NSString stringWithFormat:API_CATEGORY, categoryID];

    [[[self sharedManager] manager] GET:apiTarget parameters:[request kp_appendToken] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject kp_responseSuccess]) {
            NSArray *items = responseObject[@"data"];
            NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:[items count]];

            NSDateFormatter *dateStringParser = [[NSDateFormatter alloc] init];
            [dateStringParser setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"];

            for (NSDictionary *itemDictionary in items) {
                KPCategoryItem *item = [KPCategoryItem new];

                item.itemID = KP_ESCAPE_NSNULL(itemDictionary[@"id"]);
                item.author = KP_ESCAPE_NSNULL(itemDictionary[@"author"]);
                item.title = KP_ESCAPE_NSNULL(itemDictionary[@"title"]);
                item.categoryName = KP_ESCAPE_NSNULL(itemDictionary[@"category_name"]);
                item.thumbnail = [KP_ESCAPE_NSNULL(itemDictionary[@"thumbnail"]) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                item.pageURL = KP_ESCAPE_NSNULL(itemDictionary[@"url"]);
                item.content = KP_ESCAPE_NSNULL(itemDictionary[@"plain_content"]);
                item.htmlContent = KP_ESCAPE_NSNULL(itemDictionary[@"content"]);

                item.createDate = [dateStringParser dateFromString:KP_ESCAPE_NSNULL(itemDictionary[@"post_date"])];
                item.modifyDate = [dateStringParser dateFromString:KP_ESCAPE_NSNULL(itemDictionary[@"last_modify"])];

                [dataArray addObject:item];
            }

            NSInteger totalResult = [[responseObject valueForKeyPath:@"pageInfo.totalResults"] integerValue];

            handler(dataArray, nil, ((page + 1) * LOAD_PER_PAGE) < totalResult);
        } else {
            handler(nil, [NSError errorWithDomain:KPNetworkErrorDomain code:[responseObject kp_responseErrorCode] userInfo:nil], NO);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(nil, error, NO);
    }];
}

+ (NSURL *)categoryDataURL
{
    NSURL *url = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    url = [url URLByAppendingPathComponent:@"categoryData.plist"];

    return url;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSURL *baseURL = [NSURL URLWithString:KP_TAIPEI_BASE_URL];
        baseURL = [baseURL URLByAppendingPathComponent:KP_TAIPEI_API_VERSION];

        // configure manager
        _manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    return self;
}

@end
