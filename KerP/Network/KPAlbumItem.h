//
//  KPAlbumItem.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/25.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KPAlbumItem : NSObject

@property (nonatomic, strong) NSString *albumID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSDate *createDate;
@property (nonatomic, strong) NSDate *updateDate;

@property (nonatomic, assign) NSInteger numberOfPhotos;
@property (nonatomic, assign) NSInteger numberOfVideos;

@property (nonatomic, strong) NSString *thumbnailImageLocation;

@end
