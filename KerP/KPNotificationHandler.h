//
//  KPNotificationHandler.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/19.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPNotificationHandler : NSObject

+ (instancetype)sharedHandler;

@property (nonatomic, assign, readonly) BOOL handling;
@property (nonatomic, copy, readonly) NSNumber *itemID;

- (void)handleURL:(NSURL *)url;
- (void)setEventAction:(void (^)(void))action;
- (void)performEvent;

@end
