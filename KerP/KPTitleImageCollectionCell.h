//
//  KPTitleImageCollectionCell.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/1.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPTitleImageCollectionCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *titleImageView;

@end
