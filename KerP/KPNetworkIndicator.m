//
//  KPNetworkIndicator.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/25.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import "KPNetworkIndicator.h"

@implementation KPNetworkIndicator {
    NSTimer *animationTimer;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.userInteractionEnabled = NO;
    }

    return self;
}

- (void)fireTimer:(NSTimer *)timer
{
    self.currentPage = (self.currentPage + 1) % self.numberOfPages;
}

- (void)startAnimation
{
    animationTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(fireTimer:) userInfo:nil repeats:YES];
}

- (void)stopAnimation
{
    [animationTimer invalidate];
    animationTimer = nil;
}

@end
