//
//  KPTitleImageCollectionCell.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/1.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "KPTitleImageCollectionCell.h"

@implementation KPTitleImageCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
}

@end
