//
//  KPMenuController.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/1.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

@import iAd;

extern const CGFloat menuRowHeight;

typedef NS_ENUM(NSInteger, KPMenuItem) {
    KPMenuItemCategory = 0,
    KPMenuItemScheduleImage,
    KPMenuItemSounds,
    KPMenuItemVideos,
};

@interface KPMenuController : UIViewController <UICollectionViewDataSource, ADBannerViewDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet ADBannerView *adView;

@end
