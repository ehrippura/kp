//
//  KPMenuController.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/1.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "KPMenuController.h"
#import "KPAppDelegate.h"
#import "UIWindow+Blur.h"
#import "KPTitleImageCollectionCell.h"
#import "KPNavigationViewController.h"
#import "UIImageEffects.h"

const CGFloat menuRowHeight = 48.f;

@interface KPMenuController () {
    NSArray *itemValues;
    UIButton *closeButton;
}

@end

@implementation KPMenuController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        itemValues = @[
            NSLocalizedString(@"Category", nil),
            NSLocalizedString(@"Schedule Images", nil),
//            NSLocalizedString(@"Sounds", nil),
//            NSLocalizedString(@"Videos", nil),
        ];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.view setTintColor:[UIColor whiteColor]];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.scrollEnabled = NO;

    // create dismiss button
    closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.contentMode = UIViewContentModeCenter;
    [closeButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(dismiss:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];

    closeButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;

    self.adView.hidden = !self.adView.bannerLoaded;

    self.canDisplayBannerAds = YES;
    self.definesPresentationContext = YES;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    self.collectionView.contentInset = UIEdgeInsetsZero;
    closeButton.frame = CGRectMake(self.view.bounds.size.width - 50, 40, 33, 33);
}

- (void)dismiss:(id)sender
{
    KPNavigationViewController *navController = (KPNavigationViewController *)self.parentViewController;
    [navController dismissMenu];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - data source
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [itemValues count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    KPTitleImageCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];

    cell.titleLabel.text = itemValues[indexPath.row];
    cell.titleImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"menu%ld", (long)indexPath.row]];

    cell.alpha = 0;
    cell.transform = CGAffineTransformMakeTranslation(0, -20);

    [UIView animateWithDuration:0.3 delay:(indexPath.item + 1) * 0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
        cell.alpha = 1;
        cell.transform = CGAffineTransformIdentity;
    } completion:nil];

    return cell;
}

#pragma mark - AD Banner
- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    banner.hidden = NO;

    CGRect bannerFrame = banner.frame;
    CGRect originalFrame = bannerFrame;

    bannerFrame.origin.y += bannerFrame.size.height;
    banner.frame = bannerFrame;

    [UIView animateWithDuration:0.25 animations:^{
        banner.frame = originalFrame;
    }];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"%@", error);
}

@end
