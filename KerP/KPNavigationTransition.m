//
//  KPNavigationTransition.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/25.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import "KPNavigationTransition.h"

@implementation KPNavigationTransition

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 0.3;
}

// This method can only  be a nop if the transition is interactive and not a percentDriven interactive transition.
- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    UIView *containView = [transitionContext containerView];

    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];

    toViewController.view.alpha = 0;
    [containView insertSubview:toViewController.view belowSubview:fromViewController.view];

    [UIView animateKeyframesWithDuration:0.3 delay:0 options:0 animations:^{

        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:1.0 animations:^{
            toViewController.view.alpha = 1;
            fromViewController.view.alpha = 0;
        }];

        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:1.0 animations:^{
            fromViewController.view.transform = CGAffineTransformMakeTranslation(0, fromViewController.view.bounds.size.height);
        }];

    } completion:^(BOOL finished) {
        fromViewController.view.transform = CGAffineTransformIdentity;
        fromViewController.view.alpha = 1;
        [containView addSubview:toViewController.view];
        [transitionContext completeTransition:YES];
    }];
}

@end
