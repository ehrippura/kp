//
//  main.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/21.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KPAppDelegate class]));
    }
}
