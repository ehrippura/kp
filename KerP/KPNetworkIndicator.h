//
//  KPNetworkIndicator.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/25.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPNetworkIndicator : UIPageControl

- (void)startAnimation;
- (void)stopAnimation;

@end
