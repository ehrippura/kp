//
//  KPCategoryViewController.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/1.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "KPCategoryViewController.h"
#import "KPCategoryItemCell.h"
#import "KPNetworkManager.h"
#import "KPCategoryItem.h"
#import "KPTitleImageCollectionCell.h"
#import "UIImageView+AFNetworking.h"
#import "KPCategoryItemViewController.h"
#import "KPNotificationHandler.h"
#import "NSIndexPath+KPCategory.h"

@interface KPCategoryViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {
    NSMutableArray *newsData;
    NSInteger selectedCategory;
    NSInteger selectedCategoryIndex;

    BOOL categoryInitializing;
    BOOL hasMoreToLoad;
    BOOL networkLoading;

    NSInteger loadedPageNumber;

    __weak id notificationHitItem;
}

@property (nonatomic, strong, readonly) NSArray *categories;

- (void)setCategory:(KPCategory *)category;
- (void)loadDataFromNetwork;

@end

@implementation KPCategoryViewController

@synthesize categories = _categories;

- (void)viewDidLoad
{
    [super viewDidLoad];

    loadedPageNumber = 0;

    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.separatorColor = [UIColor colorWithRed:0.519f green:0.239f blue:0.076f alpha:1.000f];

    self.categoryListView.scrollsToTop = NO;
    
    self.clearsSelectionOnViewWillAppear = YES;
    [self setCategory:[self.categories firstObject]];

    categoryInitializing = NO;
    hasMoreToLoad = YES;
    networkLoading = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if ([self.categories count] != 0)
        [self.categoryListView selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionLeft];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    _categories = nil;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (NSArray *)categories
{
    if (!_categories) {
        NSURL *url = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        url = [url URLByAppendingPathComponent:@"categoryData.plist"];

        if ([[NSFileManager defaultManager] fileExistsAtPath:url.path])
            _categories = [NSKeyedUnarchiver unarchiveObjectWithFile:url.path];

        if (!categoryInitializing && !_categories) {
            categoryInitializing = YES;
            [KPNetworkManager requireCategoryWithCompleteHandler:^(NSArray *objectList, NSError *error, BOOL hasMore) {
                if (!error) {
                    NSURL *url = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
                    url = [url URLByAppendingPathComponent:@"categoryData.plist"];
                    [NSKeyedArchiver archiveRootObject:objectList toFile:url.path];
                }

                categoryInitializing = NO;
                [self.categoryListView reloadData];
                [self setCategory:[self.categories firstObject]];
            }];
        }
    }

    return _categories;
}

- (void)setCategory:(KPCategory *)category
{
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    newsData = [[NSMutableArray alloc] init];
    selectedCategory = [category.itemID integerValue];
    [self.tableView reloadData];

    hasMoreToLoad = YES;
    loadedPageNumber = 0;
    if (category) {
        self.navigationItem.title = category.name;
        [self loadDataFromNetwork];
    }
}

- (void)loadDataFromNetwork
{
    if (hasMoreToLoad && !networkLoading) {
        networkLoading = YES;
        [KPNetworkManager requireItemsForCategoryID:@(selectedCategory)
                                         pageNumber:loadedPageNumber++
                                    completehandler:^(NSArray *objectList, NSError *error, BOOL hasMore) {
                                        if (!error) {
                                            NSInteger originalCount = [newsData count];

                                            [newsData addObjectsFromArray:objectList];
                                            hasMoreToLoad = hasMore;

                                            [self performTodayEvent];

                                            NSArray *indexPaths = [NSIndexPath indexPathsForSection:0 fromRow:originalCount length:[objectList count]];
                                            [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
                                        }

                                        networkLoading = NO;
                                    }];
    }
}

- (void)performTodayEvent
{
    if ([KPNotificationHandler sharedHandler].handling && [newsData count] != 0) {

        if ([self.navigationController visibleViewController] != self) {
            [self.navigationController popToViewController:self animated:NO];
        }

        [newsData enumerateObjectsUsingBlock:^(KPCategoryItem *obj, NSUInteger idx, BOOL *stop) {
            if ([obj.itemID isEqualToNumber:[KPNotificationHandler sharedHandler].itemID]) {
                notificationHitItem = obj;
                [self performSegueWithIdentifier:@"performDetail" sender:nil];
                [[KPNotificationHandler sharedHandler] handleURL:nil];
                *stop = YES;
            }
        }];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    tableView.separatorStyle = ([newsData count] == 0) ? UITableViewCellSeparatorStyleNone : UITableViewCellSeparatorStyleSingleLine;

    return [newsData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = (indexPath.row) == 0 ? @"LargeCell" : @"SmallCell";
    KPCategoryItemCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];

    KPCategoryItem *item = newsData[indexPath.row];

    NSURL *imageURL = [NSURL URLWithString:item.thumbnail];

    __weak KPCategoryItemCell *weakCell = cell;
    [cell.titleImageView setImageWithURLRequest:[NSURLRequest requestWithURL:imageURL] placeholderImage:nil success:nil failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        NSLog(@"load image faliled: %@", imageURL);
        weakCell.errorImageView.hidden = NO;
    }];
    cell.titleLabel.text = [item trimedTitle];
    if (cell.descriptionLabel)
        cell.descriptionLabel.text = [item linksRemovedContent];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    KPCategoryItemCell *categoryCell = (KPCategoryItemCell *)cell;
    KPCategoryItem *item = newsData[indexPath.row];

    if (item.thumbnail == nil) {
        categoryCell.imagePlaceholderConstraint.constant = 0;
    }

    if (indexPath.row >= [newsData count] - 5) {
        [self loadDataFromNetwork];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KPCategoryItem *item = newsData[indexPath.row];
    return (indexPath.row == 0) ? (item.thumbnail == nil) ? 130 : 310 : 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.categoryListView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

#pragma mark - Collection View
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.categories count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    KPTitleImageCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TitleCell" forIndexPath:indexPath];

    cell.titleLabel.text = [self.categories[indexPath.row] name];
    cell.titleLabel.textColor = (indexPath.row == selectedCategoryIndex) ? [UIColor cyanColor] : [UIColor colorWithRed:1.000f green:0.646f blue:0.359f alpha:1.000f];

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *name = [self.categories[indexPath.row] name];
    CGRect rect = [name boundingRectWithSize:CGSizeMake(MAXFLOAT, 33) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil];

    return CGSizeMake(fabs(rect.size.width), 33);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    KPCategory *category = self.categories[indexPath.item];
    selectedCategoryIndex = indexPath.row;
    [collectionView reloadData];
    [self setCategory:category];
}

#pragma mark - Transition
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSInteger index = [self.tableView indexPathForSelectedRow].row;
    KPCategoryItem *item = notificationHitItem ?: newsData[index];

    KPCategoryItemViewController *controller = (KPCategoryItemViewController *)segue.destinationViewController;
    controller.categoryItem = item;
}

@end
