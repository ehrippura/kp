//
//  KPCategoryViewController.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/1.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPCategoryViewController : UITableViewController

@property (nonatomic, weak) IBOutlet UICollectionView *categoryListView;

- (void)performTodayEvent;

@end
