//
//  KPCategoryItemViewController.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/3.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "KPCategoryItemViewController.h"
#import "KPCategoryItem.h"
#import "YTPlayerView.h"
#import "KPCategoryItemCell.h"
#import "KPCategoryItemHeaderCell.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageEffects.h"

#import "KerP-Swift.h"

@interface KPCategoryItemViewController ()  <YTPlayerViewDelegate> {
    BOOL _videoReady;
}

@property (nonatomic, strong) UIImage *thumbnail;

@end

@implementation KPCategoryItemViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.categoryItem != nil)
        [self updateInformation];
}

- (void)updateInformation
{
    NSURL *imageURL = [NSURL URLWithString:self.categoryItem.thumbnail];
    __weak typeof (self) weakSelf = self;
    [self.backImageView setImageWithURLRequest:[NSURLRequest requestWithURL:imageURL] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        weakSelf.backImageView.image = image;
        weakSelf.blurBackImageView.image = [UIImageEffects imageByApplyingBlurToImage:image
                                                                       withRadius:10
                                                                        tintColor:[UIColor colorWithRed:0.651f green:0.342f blue:0.108f alpha:0.73]
                                                            saturationDeltaFactor:1 maskImage:nil];
        weakSelf.thumbnail = image;
    } failure:nil];

    if ([_categoryItem videoID] == nil)
        _videoReady = NO;
    else {
        [self.playerView loadWithVideoId:[self.categoryItem videoID]];
        self.playerView.delegate = self;
    }

    [self.tableView reloadData];
}

- (void)setCategoryItem:(KPCategoryItem *)categoryItem
{
    if (categoryItem != _categoryItem) {
        [self willChangeValueForKey:@"categoryItem"];

        _categoryItem = categoryItem;
        [self updateInformation];

        [self didChangeValueForKey:@"categoryItem"];
    }
}

- (IBAction)playVideo:(id)sender
{
    [self.playerView playVideo];
}

- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView
{
    _videoReady = YES;
    if ([self.tableView.visibleCells count] > 1) {
        __unsafe_unretained KPCategoryItemHeaderCell *itemCell = (KPCategoryItemHeaderCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        itemCell.playVideoButton.enabled = YES;
        [itemCell.playVideoButton setTitle:NSLocalizedString(@"Play Video", nil) forState:UIControlStateNormal];
    }
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = (indexPath.row == 0) ? @"TitleCell" : @"BodyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];

    if (indexPath.row == 0) {
        NSDateFormatter *df = [NSDateFormatter new];
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

        __unsafe_unretained KPCategoryItemHeaderCell *itemCell = (KPCategoryItemHeaderCell *)cell;

        itemCell.titleLabel.text = self.categoryItem.title;
        itemCell.dateLabel.text = [df stringFromDate:self.categoryItem.createDate];
        itemCell.authorLabel.text = self.categoryItem.author;
        itemCell.playVideoButton.layer.cornerRadius = 2;
        itemCell.playVideoButton.layer.borderColor = [UIColor whiteColor].CGColor;
        itemCell.playVideoButton.layer.borderWidth = 1;

        if ([_categoryItem videoID]) {
            itemCell.playVideoButton.hidden = NO;
            itemCell.playVideoButton.enabled = _videoReady;

            if (!_videoReady) {
                [itemCell.playVideoButton setTitle:NSLocalizedString(@"Loading", nil) forState:UIControlStateNormal];
            }
        } else {
            itemCell.playVideoButton.hidden = YES;
        }

        if (![[itemCell.playVideoButton allTargets] containsObject:self]) {
            [itemCell.playVideoButton addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        }

    } else {
        __unsafe_unretained KPCategoryItemCell *itemCell = (KPCategoryItemCell *)cell;
        itemCell.descriptionLabel.text = [self.categoryItem linksRemovedContent];
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat (^calculateStringHeight)(id, id) = ^CGFloat (NSString *str, UIFont *font) {
        CGSize limit = CGSizeMake(tableView.bounds.size.width - 40, MAXFLOAT);
        CGRect rect =
        [str boundingRectWithSize:limit
                          options:(NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin)
                       attributes:@{NSFontAttributeName: font} context:nil];

        return ceilf(rect.size.height);
    };
    
    if (indexPath.row == 0)
        return 150;

    CGFloat additional = calculateStringHeight([self.categoryItem linksRemovedContent], [UIFont systemFontOfSize:17]);

    return 43 + additional;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.row == 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"WebViewSegue" sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

static CGFloat fullOffset = 50;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offset = scrollView.contentOffset.y;
    if (offset <= 0) {
        self.imageHeightConstraint.constant = 150 - offset;

        if (self.thumbnail) {
            __unsafe_unretained UITableViewCell *firstCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            CGFloat percentage = -offset / fullOffset;
            self.blurBackImageView.alpha =
            firstCell.contentView.alpha = 1 - percentage;
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"WebViewSegue"]) {
        KPWebViewController *webView = segue.destinationViewController;
        webView.urlString = self.categoryItem.pageURL;
        webView.articalTitle = self.categoryItem.title;
    }
}

@end
