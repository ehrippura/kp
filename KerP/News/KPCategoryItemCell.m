//
//  KPCategoryItemCell.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/1.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "KPCategoryItemCell.h"

@implementation KPCategoryItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.titleImageView.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse
{
    [super prepareForReuse];

    self.titleImageView.image = nil;
    self.errorImageView.hidden = YES;
    self.imagePlaceholderConstraint.constant = (self.tag == 0) ? 180 : 120;
}

@end
