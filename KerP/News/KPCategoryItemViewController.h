//
//  KPCategoryItemViewController.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/3.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KPCategoryItem;
@class YTPlayerView;


@interface KPCategoryItemViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

// ui
@property (nonatomic, strong) IBOutlet UIImageView *backImageView;
@property (nonatomic, strong) IBOutlet UIImageView *blurBackImageView;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *imageHeightConstraint;

@property (nonatomic, strong) IBOutlet YTPlayerView *playerView;

// member
@property (nonatomic, weak) KPCategoryItem *categoryItem;

- (IBAction)playVideo:(id)sender;

@end
