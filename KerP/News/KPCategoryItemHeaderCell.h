//
//  KPCategoryItemHeaderCell.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/4.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPCategoryItemHeaderCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *authorLabel;
@property (nonatomic, weak) IBOutlet UIButton *playVideoButton;

@end
