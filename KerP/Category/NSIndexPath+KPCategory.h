//
//  NSIndexPath+KPCategory.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/08/27.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSIndexPath (KPCategory)

+ (NSArray *)indexPathsForSection:(NSInteger)section fromRow:(NSInteger)row length:(NSInteger)length;

@end
