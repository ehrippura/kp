//
//  UIViewController+Blur.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/1.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (Blur)

- (void)enableBlurWithOverlayView:(UIView *)view presentingAnimation:(void (^)())animation;
- (void)disableBlur;

@end
