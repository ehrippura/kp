//
//  UIViewController+Blur.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/1.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "UIWindow+Blur.h"
#import "UIImageEffects.h"

#import <objc/runtime.h>

const char * const blurredImageViewKey = "tw.eternalwind.kerp.associated.blurredImageView";

@interface UIWindow ()

@property (nonatomic, strong, setter = kp_setBlurredImageView:) UIImageView *kp_blurredImageView;
@end

@implementation UIWindow (Blur)

- (void)enableBlurWithOverlayView:(UIView *)view presentingAnimation:(void (^)())animation
{
    CGSize targetSize = self.bounds.size;
    UIGraphicsBeginImageContext(targetSize);

    [self drawViewHierarchyInRect:(CGRect){.origin = {0, 0}, .size = targetSize} afterScreenUpdates:NO];
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    screenshot = [UIImageEffects imageByApplyingDarkEffectToImage:screenshot];

    UIImageView *blurBackground = [[UIImageView alloc] initWithImage:screenshot];
    blurBackground.userInteractionEnabled = YES;
    blurBackground.alpha = 0;

    view.frame = blurBackground.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

    [blurBackground addSubview:view];
    [self.rootViewController.view addSubview:blurBackground];

    [UIView animateWithDuration:0.3 animations:^{
        blurBackground.alpha = 1;
        if (animation != nil)
            animation();
    }];

    self.kp_blurredImageView = blurBackground;
}

- (void)disableBlur
{
    [UIView animateWithDuration:0.3 animations:^{
        self.kp_blurredImageView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.kp_blurredImageView removeFromSuperview];
        self.kp_blurredImageView = nil;
    }];
}

- (void)kp_setBlurredImageView:(UIImageView *)imageView
{
    objc_setAssociatedObject(self, blurredImageViewKey, imageView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIImageView *)kp_blurredImageView
{
    return objc_getAssociatedObject(self, blurredImageViewKey);
}



@end
