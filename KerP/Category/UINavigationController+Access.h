//
//  UINavigationController+Access.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/22.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Access)

- (UIViewController *)kp_rootViewController;

@end
