//
//  NSIndexPath+KPCategory.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/08/27.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "NSIndexPath+KPCategory.h"

@implementation NSIndexPath (KPCategory)

+ (NSArray *)indexPathsForSection:(NSInteger)section fromRow:(NSInteger)row length:(NSInteger)length
{
    NSMutableArray *mutarray = [[NSMutableArray alloc] initWithCapacity:length];

    for (NSInteger i = row; i < length + row; i++) {
        [mutarray addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }

    return mutarray;
}

@end
