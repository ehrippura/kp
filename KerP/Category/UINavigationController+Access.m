//
//  UINavigationController+Access.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/22.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "UINavigationController+Access.h"

@implementation UINavigationController (Access)

- (UIViewController *)kp_rootViewController
{
    return [self.viewControllers firstObject];
}
@end
