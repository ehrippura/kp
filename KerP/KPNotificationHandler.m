//
//  KPNotificationHandler.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/19.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "KPNotificationHandler.h"

@interface KPNotificationHandler () {
    void (^eventAction)(void);
}

@end

@implementation KPNotificationHandler

+ (instancetype)sharedHandler
{
    static id master = nil;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        master = [self new];
    });

    return master;
}

- (void)handleURL:(NSURL *)url
{
    if ([[url scheme] isEqualToString:@"ewkerptaipei"]) {
        _handling = YES;
        _itemID = @([[url lastPathComponent] integerValue]);
    } else {
        _handling = NO;
        _itemID = nil;
    }
}

- (void)setEventAction:(void (^)(void))action
{
    eventAction = action;
}

- (void)performEvent
{
    if (eventAction) {
        eventAction();
    }
}

@end
