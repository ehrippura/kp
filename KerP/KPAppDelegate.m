//
//  KPAppDelegate.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/21.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import "KPAppDelegate.h"
#import "KPNavigationDelegate.h"
#import "KPNetworkManager.h"
#import "KPNotificationHandler.h"
#import "AFNetworkActivityIndicatorManager.h"

@implementation KPAppDelegate {
    KPNavigationDelegate *_navDelegate;
}

+ (void)initialize
{
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    _navDelegate = [KPNavigationDelegate new];

    self.rootNavigationController.delegate = _navDelegate;
    self.mainStoryboard = [UIStoryboard storyboardWithName:[[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"]
                                                    bundle:nil];

    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [[KPNotificationHandler sharedHandler] handleURL:url];
    [[KPNotificationHandler sharedHandler] performEvent];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Getter/Setter
- (UINavigationController *)rootNavigationController
{
    return (UINavigationController *)self.window.rootViewController;
}

@end
