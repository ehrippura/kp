//
//  KPNavigationTransition.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/25.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KPNavigationTransition : NSObject <UIViewControllerAnimatedTransitioning>

@end
