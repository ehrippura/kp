//
//  KPNavigationViewController.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/1.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "KPNavigationViewController.h"
#import "KPMenuController.h"
#import "UIWindow+Blur.h"
#import "KPAppDelegate.h"
#import "KPNotificationHandler.h"
#import "KPCategoryViewController.h"

#import "Category/UINavigationController+Access.h"

@interface KPNavigationViewController () <UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) KPMenuController *menuController;

- (void)handleNotificationEvent;

@end

@implementation KPNavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    __weak typeof(self) weakSelf = self;  // prevent retain cycle
    [[KPNotificationHandler sharedHandler] setEventAction:^{
        [weakSelf handleNotificationEvent];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)presentMenu
{
    __unsafe_unretained KPAppDelegate *appDelegate = (KPAppDelegate *)[UIApplication sharedApplication].delegate;
    self.menuController = [appDelegate.mainStoryboard instantiateViewControllerWithIdentifier:@"MenuController"];
    [self.menuController view]; // force load view

    self.menuController.collectionView.delegate = self;
    [self.menuController.collectionView.collectionViewLayout invalidateLayout];

    __unsafe_unretained UIView *menuView = self.menuController.view;
    [self addChildViewController:self.menuController];

    [[UIApplication sharedApplication].keyWindow enableBlurWithOverlayView:menuView presentingAnimation:nil];
}

- (BOOL)shouldAutorotate
{
    return [[self visibleViewController] shouldAutorotate];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return [[self visibleViewController] supportedInterfaceOrientations];
}

- (void)dismissMenu
{
    [self.menuController removeFromParentViewController];
    self.menuController = nil;
    [[UIApplication sharedApplication].keyWindow disableBlur];
}

- (void)handleNotificationEvent
{
    if (![[self kp_rootViewController] isKindOfClass:[KPCategoryViewController class]]) {
        [self switchToViewControllerWithMenuItem:KPMenuItemCategory];
    }

    KPCategoryViewController *categoryController = (KPCategoryViewController *) [self kp_rootViewController];
    [categoryController performTodayEvent];
}

- (void)switchToViewControllerWithMenuItem:(KPMenuItem)item
{
    __unsafe_unretained KPAppDelegate *appDelegate = (KPAppDelegate *)[UIApplication sharedApplication].delegate;
    NSString *storyboardIdentifier = nil;
    switch (item) {
        case KPMenuItemCategory:
            storyboardIdentifier = @"CategoryView";
            break;

        case KPMenuItemScheduleImage:
            storyboardIdentifier = @"AlbumList";
            break;

        default:
            break;
    }

    UIViewController *viewController = [appDelegate.mainStoryboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
    self.viewControllers = @[viewController];
}

#pragma mark - delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissMenu];
    [self switchToViewControllerWithMenuItem:(KPMenuItem)indexPath.row];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width, 100);
}

@end
