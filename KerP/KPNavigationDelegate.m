//
//  KPNavigationDelegate.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/8/25.
//  Copyright (c) 2014 Eternal Wind. All rights reserved.
//

#import "KPNavigationDelegate.h"
#import "KPNavigationTransition.h"
#import "KPNavigationViewController.h"
#import "KPMenuController.h"

@implementation KPNavigationDelegate {
    KPNavigationTransition *_transition;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _transition = [KPNavigationTransition new];
    }
    return self;
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    // set left navigation bar item
    if (viewController == navigationController.viewControllers[0] && !viewController.navigationItem.leftBarButtonItem) {
        UIBarButtonItem *barItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] landscapeImagePhone:nil style:UIBarButtonItemStylePlain target:self action:@selector(presentMenu:)];
        viewController.navigationItem.leftBarButtonItem = barItem;
    }
}

- (void)navigationController:(UINavigationController *)navigationController
       didShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{

}

- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                   animationControllerForOperation:(UINavigationControllerOperation)operation
                                                fromViewController:(UIViewController *)fromVC
                                                  toViewController:(UIViewController *)toVC
{
    return _transition;
}

- (void)presentMenu:(id)sender
{
    KPNavigationViewController *navController = (KPNavigationViewController *)[UIApplication sharedApplication].keyWindow.rootViewController;

    [navController presentMenu];
}

@end
