//
//  TodayViewController.m
//  KerPToday
//
//  Created by Tzu-Yi Lin on 2014/9/18.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>

#import "AFNetworking.h"
#import "KPTodayTableCell.h"
#import "KPCategoryItem.h"
#import "KPNetworkManager.h"

#import "KerPToday-Swift.h"

#define SCHEME @"ewkerptaipei://"

@interface TodayViewController () <NCWidgetProviding> {
    NSArray *data;
}

@property (nonatomic, copy) void (^handler)(NCUpdateResult);

@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.tableView.rowHeight = 80;
    NSData *savedData = [[KPUserDefault standardUserDefaults] objectForKey:@"savedItemData"];
    if (savedData)
        data = [NSKeyedUnarchiver unarchiveObjectWithData:savedData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    if (self.handler) {
        self.handler(NCUpdateResultFailed);
        self.handler = nil;
    }

    [[KPUserDefault standardUserDefaults] synchronize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.

    self.handler = completionHandler;
    [self updateFromNetwork];
}

- (BOOL)hasNewDataInDataArray:(NSArray *)dataArray
{
    if ([data count] == 0) {
        return YES;
    }

    for (KPCategoryItem *item in data) {
        if (![dataArray containsObject:item])
            return YES;
    }

    return NO;
}

- (void)updateFromNetwork
{
    if (!self.handler) return;

    [KPNetworkManager requireItemsForCategoryID:@(40) pageNumber:0 completehandler:^(NSArray *objectList, NSError *error, BOOL hasMore) {

        if (!self.handler) return;

        if (error) {
            [self setPreferredContentSize: CGSizeMake(self.tableView.frame.size.width, 1)];
            self.handler(NCUpdateResultNoData);
        } else {

            NSArray *newData;
            if ([objectList count] > 3) {
                newData = [objectList subarrayWithRange:NSMakeRange(0, 3)];
            } else {
                newData = objectList;
            }

            if ([self hasNewDataInDataArray:newData]) {
                data = newData;
                [self.tableView reloadData];
                NSData *dataToSave = [NSKeyedArchiver archivedDataWithRootObject:data];
                [[KPUserDefault standardUserDefaults] setObject:dataToSave forKey:@"savedItemData"];
                self.handler(NCUpdateResultNewData);
            } else {
                self.handler(NCUpdateResultNoData);
            }
        }

        self.handler = nil;
    }];
}

- (UIEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(UIEdgeInsets)defaultMarginInsets
{
    return UIEdgeInsetsMake(defaultMarginInsets.top, defaultMarginInsets.left, 0, defaultMarginInsets.right);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    [self setPreferredContentSize:CGSizeMake(self.tableView.frame.size.width, ([data count] != 0) ? (80 * [data count] - 1) : 1)];
    return [data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KPTodayTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    KPCategoryItem *item = data[indexPath.row];

    cell.contentLabel.text = item.linksRemovedContent;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSURL *targetURL = [NSURL URLWithString:[SCHEME stringByAppendingString:@"40"]];

    KPCategoryItem *item = data[indexPath.row];
    targetURL = [targetURL URLByAppendingPathComponent:item.itemID.stringValue];

    [self.extensionContext openURL:targetURL completionHandler:nil];
}

@end
