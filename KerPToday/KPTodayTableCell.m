//
//  KPTodayTableCell.m
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/19.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import "KPTodayTableCell.h"

@implementation KPTodayTableCell

- (void)layoutSubviews
{
    [super layoutSubviews];

    self.imageView.frame = CGRectMake(0, 0, self.contentView.bounds.size.height, self.contentView.bounds.size.height);
    self.imageView.layer.cornerRadius = self.imageView.bounds.size.width / 2;
}

@end
