//
//  KPTodayTableCell.h
//  KerP
//
//  Created by Tzu-Yi Lin on 2014/9/19.
//  Copyright (c) 2014年 Eternal Wind. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPTodayTableCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *contentLabel;

@end
